fLask
=====

## Contenido

- Prefacio
- Qué es micro
- Configuracion y convenciones
- Creciedo con Flask
- Version de Python
- Dependencias
- Dependencias opcionales
- Virtual enviroment
- Instalar Flask

## Prefacio

Lea esto antes de comenzar con Flask. Es de esperar que responda algunas 
preguntas sobre el propósito y los objetivos del proyecto, y cuándo debería 
o no debería usarlo.

## ¿Qué significa "micro"?

"Micro" no significa que toda su aplicación web tenga que caber en un solo 
archivo de Python (aunque ciertamente puede), ni tampoco significa que Flask 
carezca de funcionalidad. El "micro" en microframework significa que Flask 
pretende mantener el núcleo simple pero extensible. Flask no tomará muchas 
decisiones por usted, como qué base de datos usar. Las decisiones que toma, 
como qué motor de creación de plantillas utilizar, son fáciles de cambiar. 
Todo lo demás depende de ti, para que Flask pueda ser todo lo que necesites 
y nada que no necesites.

De forma predeterminada, Flask no incluye una capa de abstracción de base de 
datos, validación de formulario ni nada más, donde ya existen diferentes 
bibliotecas que pueden manejar eso. En cambio, Flask admite extensiones para 
agregar dicha funcionalidad a su aplicación como si fuera implementada en el 
Flask mismo. Numerosas extensiones proporcionan integración de bases de 
datos, validación de formularios, manejo de cargas, diversas tecnologías de 
autenticación abierta, y más. El matraz puede ser "micro", pero está listo 
para usarse en la producción en una variedad de necesidades.

## Configuración y Convenciones

Flask tiene muchos valores de configuración, con valores predeterminados 
razonables y algunas convenciones al comenzar. Por convención, las plantillas 
y los archivos estáticos se almacenan en subdirectorios dentro del árbol 
fuente de Python de la aplicación, con las plantillas de nombres y la 
estática respectivamente. Si bien esto se puede cambiar, generalmente no es 
necesario, especialmente al comenzar.

## Creciendo con Flask

Una vez que tenga Flask en funcionamiento, encontrará una variedad de 
extensiones disponibles en la comunidad para integrar su proyecto para la 
producción. El equipo central de Flask revisa las extensiones y garantiza 
que las extensiones aprobadas no se rompan con versiones futuras.

A medida que su base de código crece, puede tomar decisiones de diseño 
apropiadas para su proyecto. Flask continuará proporcionando una capa de 
pegamento muy simple a lo mejor que Python tiene para ofrecer. Puede 
implementar patrones avanzados en SQLAlchemy u otra herramienta de base de 
datos, introducir la persistencia de datos no relacionales según corresponda 
y aprovechar las herramientas independientes del marco de trabajo construidas 
para WSGI, la interfaz web de Python.

Flask incluye muchos ganchos para personalizar su comportamiento. Si necesita 
más personalización, la clase Flask está diseñada para la creación de 
subclases. Si estás interesado en eso, mira el capítulo Becoming Big. Si 
tiene curiosidad acerca de los principios de diseño de Flask, diríjase a la 
sección sobre Design Decisions in Flask.

## Versión de python

Se recomienda usar la última version de python 3, en este caso 3.7.0, pero si 
se planea usar pypy como interprete en un ambiente productivo, la versión
sobre la que se recomienda trabajar es la 3.5.x.

## Dependencias

Flask se distribuye e instala con los siguientes componentes:

#### [Werkzeug][werkzeug]
>Implementa WSGI. la interface estandar de python entre aplicaciones y servior.

#### [Jinja][Jinja]
> Un `template engine` para python.
#### [MarkupSafe][markupsafe]
> Viene con Jinja y escapa las entradas no confiables cuando de renderiza el 
template para evitar un ataque de inyección.

## dependencias opcionales

Las siguientes dependencias son recomendadas tanto por la 
[pagina oficial][flask] de flask y algunas mas que he usado y han resultado 
bastante bien para poder hacer un mejor ambiente de desarrollo.

#### [Blinker][blinker]
> Provee soporte para [Señales][signals]

#### [SimpleJSON][simplejson]
> Es una implementación rápida de JSON compatible con JSON de python.

#### [python-dotenv][python dotenv]
> Habilita el sopote para variables de entorno desde dotenv cuando se corre el comando flask.

#### [Watchdog][watchdog]
> provee una rápida y eficiente recarga desde el servidor de desarrollo.

#### [Pytest][pytest]
> Es un framework para poder escribir de forma mas facil pruebas unitarias.

#### [Redis][redis]
> Interface de python para el sistema de almacenamiento clave-valor redis.

#### [Unipath][unipath]
> Es una libreria para el manejo de paths orientado a objetos basado en la 
> implementación de Jason Orendorff.

#### [PyYaml][pyyaml]
> Es una liberia para la seriaización del formato YAML.

## Virtual enviroments

El uso de entornos virtuales facilita la administracción de de dependencias 
de proyectos que varian de versiones y que de no ser por estos entornos, 
se dificultaria mucho el desarrollo y la puesta en producción de estos 
sistemas.

¿Qué problema resuelve un entorno virtual? Cuantos más proyectos Python tenga,
es más probable que necesite trabajar con diferentes versiones de las 
bibliotecas de Python, o incluso de Python. Las versiones más recientes de 
bibliotecas para un proyecto pueden romper la compatibilidad en otro proyecto.

Los entornos virtuales son grupos independientes de bibliotecas de Python, 
uno para cada proyecto. Los paquetes instalados para un proyecto no 
afectarán a otros proyectos o paquetes del sistema operativo.

Python 3 viene incluido con el módulo venv para crear entornos virtuales. 

### Creando un entorno virtual

Para poder gestionar de una manera adecueada los entornos virtuales que se 
puedan crear, se recomenda el uso de [VirtualEnvWrapper][virtualenvwrapper]
que se describira como parte del este manual.

#### VirtualEnvWrapper

Es un conjunto de extenciones de la herramienta virtualenv creada por 
[Ian Bicking][ianbricking]. Las extensiones incluyen envoltorios para crear 
y eliminar entornos virtuales y, de lo contrario, administrar el flujo de 
trabajo de desarrollo, lo que facilita el trabajo en más de un proyecto a la 
vez sin introducir conflictos en sus dependencias.

- Organiza todos los entornos virtuales en un solo lugar.
- Contenedores para administrar sus entornos virtuales (crear, eliminar, copiar).
- Use un solo comando para cambiar entre ambientes.
- Finalización de tabulación para comandos que toman un entorno virtual como argumento.
- Hooks configurables por el usuario para todas las operaciones (ver Personalización por usuario).
- Sistema de plugins para crear más extensiones compartibles.

#### Creando un entorno virtual de python 3

La forma mas simple de crear in virtualenv en python es con el modulo de venv
que es parte del las librerias estandar de python 3.

```
python3 -m venv /path/of/virtual/enviroment
```
Otra forma es con el ya mencionado virtualenvwrapper, el cual nos permite tener
organizados los virtualenv en un solo lugar y poder usarlos sin tener que ir a
su carpeta a activarlos.

Recuerda que para instalar una libreria de python, requieres ser root, o en su
defecto, usar `--user` en pip para que solo se instale a nivel de tu usuario actual.

Dicho esto, se debe instalar virtualenvwrapper y configura las variables de 
entorno para poder usar los comandos workon y mkvirtualenv, el cual uno 
inicializa el virtual enviroment tan solo por el nombre de este, y el otro
crea en virtual enviroment en una carpeta ya especificada. virtualenvwrapper
se ayuda de la variable de ambiente `WORKON_HOME`, la cual indicará donde se
deben crear y buscar los virtual enviroments creados, esto puedes ponerlo en tu
archivo rc bashrc en caso de los que usen bash, o en tu zshrc, si gustas saber
un poco mas sobre esto, puedes leer el siguiente [articulo][profiles and rc files]
de Linux Journal.

Un ejemplo de lo anterior serían las siguientes lineas:
```
$ export WORKON_HOME=~/venvs
$ mkdir -p $WORKON_HOME
$ source /usr/bin/virtualenvwrapper.sh
$ mkvirtualenv env1
$ workon env1
```

En mi caso, `virtualenvwrapper.sh` esta en `usr/bin`, pero si no estan seguros
de donde esta, pueden usar el comando whereis o locate, el cual sirve para
buscar archivos por nombre. Otra cosa, mkvirtualenv creara un entorno para 
python 2.7, y dado a que nosotros usaremos 3.6, tendremos que agregarle la
siguiente bandera u opción `--python=python3.6`


### Recomendaciones

Aunque la mayoria de mis recomendaciones seran generales, algunas las dividire
para el entorno de desarrollo y otro para el entorno productivo dado a que este
último se debe tener mas cuidado y control.

#### Propocito de la carpeta opt en sistemas tipo UNIX

opt está reservado para la instalación de paquetes de software complementarios.

Un paquete que se instale en /opt deberá ubicar sus archivos en un arbol de 
directorios, ya sea por nombre del paquete o por nombre del proveedor.

 ```
/opt/<package>
/opt/<proveedor> 
 ```

Los directorios `/opt/bin`, `/opt/doc`, `/opt/include`, `/opt/info`, `/opt/lib`
y `/opt/man` están reservados para el uso del administrador del sistema local.

Los programas que pueden invocar los usuarios deben estar localizados en el 
directorio `/opt/<package>` o bajo el directorio `/opt/<proveedor>`. Se debe 
tomar en cuenta que el nombre del proveedor es asignado por [LANANA][lanana]

El nombre del proveedor LSB asignado por [LANANA][lanana] en base a [LSB Core specification],
y el nombre del paquete debe seguir [LSB Core pkg naming].

Otra alternativa es el uso de la carpeta `/srv` que esta designada para poder 
contener servicios en un arbol de solo lectura, en otras palabras, archivos que
no seran modificados en su ejecución. Por el momento no existe una especificación
para en nombramiento de directorios en `srv`, por lo que pueden usar 
[LSB pkg naming][LSB Core pkg naming] si gustan o que el area de trabajo defina
un nombre.


### Estructura de carpetas en el sistema.

Independientemente de la carpeta a usar en el servidor y sabiendo que sera para
sistemas escritos en python para su ejecición mediante un WSGI (Web Service Gateway Interface),
se usará el siguiente árbol de carpetas.
```
<proveedor>/
    <proyecto>/
    run/
    env/
    profile/
```

El nombre del proveedor es Enova, así que dejando ese tema y duda, explicaremos
las siguientes carpetas para poder tener una separación organizada y sea una 
regla que como empresa podamos seguir.

`<proyecto>/` Es el nombre del proyecto en python.
`run/` En esta carpeta se crearan los sockets o pid creados por el gestor de WSGI.
`env/` En esta carpeta se deberá crear los virtualEnviroment de python.
`profile\` Aquí se pondrán los script de las variables de entorno que los proyectos
necesiten y se creara un link simbolico a `/etc/profile.d`

`/etc/profile.d/` El archivo `/etc/profile` es un archivo de inicialización del
sistema para sesiones con shell, y lee profile.d para extender y ejecutar
archivos extras.


#### Estructura básica del proyecto Flask

Se tomará la siguiente estructura de carpetas para el uso de Flask en Enova.

```
<carpeta del proyecto Flask>/
    app.py
    project/
    __init__.py
    settings.py
    router.py
    modules/
        __init__.py
        <nombre del modulo>\
            __init__.py
            route.py
            controller.py
            models.py
            signals.py
            serializers.py
            helpers.py
            middlewares.py
            migrations/
                __init__.py
            commands/
                __init__.py
```



[flask]:<http://flask.pocoo.org>
[Jinja]:<http://jinja.pocoo.org/>
[werkzeug]:<http://werkzeug.pocoo.org/>
[markupsafe]:<https://pypi.org/project/MarkupSafe/>
[simplejson]:<https://simplejson.readthedocs.io/>
[itsDangerous]:<https://pythonhosted.org/itsdangerous/>
[click]:<http://click.pocoo.org/>
[pytest]:<https://pytest.org>
[blinker]:<https://pythonhosted.org/blinker/>
[signals]:<http://flask.pocoo.org/docs/1.0/signals/#signals>
[python dotenv]:<https://github.com/theskumar/python-dotenv#readme>
[watchdog]:<https://pythonhosted.org/watchdog/>
[redis]:<https://pypi.org/project/redis/>
[pytz]:<https://pypi.org/project/pytz/>
[unipath]:<https://pypi.org/project/Unipath/>
[pyyaml]:<https://pypi.org/project/PyYAML/>
[virtualenvwrapper]:<https://virtualenvwrapper.readthedocs.io>
[ianbricking]:<http://www.ianbicking.org>
[opt-fshs]:<http://www.pathname.com/fhs/pub/fhs-2.3.html#OPTADDONAPPLICATIONSOFTWAREPACKAGES>
[lanana]:<http://lanana.org/>
[LSB Core pkg naming]:<http://refspecs.linux-foundation.org/LSB_4.0.0/LSB-Core-generic/LSB-Core-generic/pkgnameconv.html>
[LSB Core specification]:<http://refspecs.linux-foundation.org/LSB_4.0.0/LSB-Core-generic/LSB-Core-generic/book1.html>
[python module index]:<https://docs.python.org/3/py-modindex.html>
[profiles and rc files]:<https://www.linuxjournal.com/content/profiles-and-rc-files>
